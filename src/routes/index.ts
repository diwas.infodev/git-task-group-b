import { lazy } from 'react';
import { externalRoute } from './DistinctRoute/External';
import { internalRoute } from './DistinctRoute/Internal';
import CheckOutForm from '../core/Public/Products/checkOutForm';
const Login = lazy(() => import("../core/Public/Login/Login"));
const Register = lazy(() => import("../core/Public/Register/Register"));

const Boundary = lazy(() => import("../core/Protected/Boundary"));
const Products = lazy (() => import("../core/Public/Products/Products"));

export const appRoutes: CustomRoute[] = [
    {
        path: "/login",
        component: Login,
        type: "unauthorized"
    },
    {
        path: "/register",
        component: Register,
        type: "unauthorized"
    },
    {
        path: "/checkout",
        component: CheckOutForm,
        type:"unauthorized"  
    },
    {
        path: "/products",
        component: Products,
        type:"unauthorized"  
    }, 
    {
        path: "/",
        component: Boundary,
        children: [...internalRoute, ...externalRoute],
    }
]
