import FormGroup from "components/React/Form/FormGroup";
import { Form, Formik, yupToFormErrors } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { toast, Toast, ToastContainer } from "react-toastify";
export const initialFormValues = {
 Name: "",
 billingAddress: "",
 deliveryAddress: "",
 mobileNumber: "",
 currentDate: "",
};
 
 
export const validationSchema = Yup.object().shape({
 Name: Yup.string().required("Name is Required"),
 billingAddress: Yup.string()
   .required("Billing address is Required"),
deliveryAddress: Yup.string()
 .required("Delivery address is Required"),
 mobileNumber: Yup.string()
   .matches(/^[0-9]+$/,"enter only number")
   .min(7, "too short")
   .max(10, "maximum number is 10")
   .required("Phone number is required"),
    currentDate: Yup.date()
    .required("Current date is Required")
});
 
const CheckOutForm: React.FunctionComponent<any> = () => {
 const[FormValues,setFormValue]= useState(initialFormValues);
  const onSubmit=(values:any,resetForm) =>{
 console.log(values,"res");
 setFormValue(values);
 toast.success('successfully checked out',{position: toast.POSITION.BOTTOM_RIGHT}, )
 resetForm();
 
 }
 
  return (
   <>
     <div className="container">
     <Formik
       initialValues={initialFormValues}
       validationSchema={validationSchema}
       onSubmit={(values, { resetForm }) => {
         onSubmit(values, resetForm);
       }}
     >
       {({
         values,
         touched,
         errors,
         handleBlur,
         handleChange,
         setFieldValue,
       }) => {
         return (
 
           <Form>
             <div>
               <h6 className="mt-3">CHECKOUT FORM</h6>
               <div className="row mt-2">
                 <div className="col-6 col-sm-6">
                   <FormGroup
                     type="text"
                     errors={errors}
                     touched={touched}
                     label="Name"
                     name="Name"
                     value={values.Name}
                     formikHandleBlur={handleBlur}
                     formikHandleChange={handleChange}
                    
                   />
                 </div>
<div className="col-6 col-sm-6">
<FormGroup
                     type="text"
                     errors={errors}
                     touched={touched}
                     label="Delivery Address"
                     name="deliveryAddress"
                     value={values.deliveryAddress}
                     formikHandleBlur={handleBlur}
                     formikHandleChange={handleChange}
                    
                   />
</div>
                 <div className="col-6 col-sm-6">
                   <FormGroup
                     type="text"
                     errors={errors}
                     touched={touched}
                     label="Billing Address"
                     name="billingAddress"
                     value={values.billingAddress}
                     formikHandleBlur={handleBlur}
                     formikHandleChange={handleChange}
                
                   />
                 </div>
                 <div className="col-6 col-sm-6">
                   <FormGroup
                     type="string"
                
                     errors={errors}
                     touched={touched}
                     label="Phone number"
                     name="mobileNumber"
                     value={values.mobileNumber}
                     formikHandleBlur={handleBlur}
                     formikHandleChange={handleChange}
                
                   />
                 </div>
                 <div className="col-4">
                   <FormGroup
                     type="DatePicker"
                     errors={errors}
                     touched={touched}
                     label="Current Date"
                     name="currentDate"
                     setFieldValue={setFieldValue}
                    formikHandleBlur={handleBlur}
                     formikHandleChange={handleChange}
                     dateSettings={
                       {
                         value: values.currentDate,
                         showYearDropdown: true,
                         maxDate: new Date(),
  }
                     }
                 
                   />
                 </div>
                
            </div>
            </div>
             <div className="divider my-3"></div>
             <div className="text-right">
             
                <button
                 //   disabled={saveAgencyData.status === 100}
                 type="reset"
                 className="btn btn-secondary  mr-3"
               >
                Reset
               </button>
               <button
                 type="submit"
                 className="btn btn-success"
               >
               submit
               </button>
             </div>
           </Form>
         );
       }}
     </Formik>
     </div>
    
   </>
 );
};
 
export default CheckOutForm;