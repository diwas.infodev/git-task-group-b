import React from 'react'
import { ProductType } from 'store/modules/products/products';


interface ProdItemInterface {
    product: ProductType;
    addToCart: any;
}

export default function ProductItem(props: ProdItemInterface) {
    const { product, addToCart } = props;
    return (
        <div>
            <div className='card'>
                <div className="product">
                    <img 
                        src={`https://electronic-ecommerce.herokuapp.com/${product.image}`} 
                        alt='products'
                    />
                    </div>
                    <span >{product.name}</span>
                    <span>Manufacture Date:{product.createDate}</span>
                    <span className='mb-3'>Category:{product.category}</span>
                    <div className="d-flex justify-content-center">
                        <button 
                            className={`${product.stock === 0 ? "btn-danger" : "btn-success"} btn mr-2`}>
                                Stock:{product.stock}
                            </button>
                        <button 
                            disabled={product.stock === 0} 
                            className='btn btn-warning'>
                                {product.price}
                            </button>
                    </div>
                    <button 
                        onClick={() => addToCart(product)}
                        disabled={product.stock === 0} 
                        className='btn btn-primary btn-xl mt-2'>
                            Add To Card
                        </button>
                
            </div>
        </div>
    )
}