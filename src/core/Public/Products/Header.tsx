import React from 'react'

interface HeaderProps {
    cartOpen: any;
    handleSearch:any
}
function Header(props: HeaderProps) {
  return (
    <div className="d-flex mx-2 px-3     pt-3 bg-light">
        <div className="mr-auto p-2">
        <div 
            style={{cursor: "pointer"}}
            className="ml-auto p-2" 
            onClick={() => props.cartOpen(true)}>
                Cart <i className='ic ic-info'></i>
        </div>
     
        </div>
        <div className="p-2">
        <div className="search--input">
                <div className="form-group-icon">
                    <input type="search" className="form-control" placeholder="PRODUCT NAME" name="name" onChange={(event)=>{
                        props.handleSearch(event)
                    }} />
                    <i className="ic-search"></i>
                </div>
                <div className="d-flex pt-3 bg-light">
                <div >
                <div className="form-group-icon">
                    <input type="search" className="form-control" placeholder="MIN PRICE" name="Minprice" onChange={(event)=>{
                        props.handleSearch(event)
                    }} />
                    <i className="ic-search"></i>
                </div>
                </div>
                <div className="pl-2">
                <div className="form-group-icon">
                    <input type="search" className="form-control" placeholder="MAX PRICE" name="Maxprice" onChange={(event)=>{
                        props.handleSearch(event)
                    }} />
                    <i className="ic-search"></i>
                </div>
                </div>
                </div>
            </div>
        </div>
        <div className="p-2">
        <div className="form-group-icon">
                    <select  className="form-control" placeholder="CATEGORY" name="category"  onChange={(event)=>{props.handleSearch(event)}} >
                    <option value="">All Category</option>
                <option value="laptop">Laptop</option>
                <option value="mobile">Mobile</option>
                <option value="Keyboard">Keyboard</option>
                <option value="headseat">Headseat</option>
                <option value="watch">Watch</option>
                </select>
                </div>
        </div>
        <div className="p-2">
        <div className="form-group-icon">
                    <input type="date" className="form-control" placeholder="DATE" name="MaxcreateDate"  max={new Date().toString()}   onChange={(event)=>{props.handleSearch(event)}} />
                </div>
        </div>
        
        
    </div>
  )
}

export default Header