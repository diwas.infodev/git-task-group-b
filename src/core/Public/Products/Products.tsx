import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { getProductsAction, ProductType } from 'store/modules/products/products';
import { RootState } from 'store/root-reducer';
import { number } from 'yup/lib/locale';
import Header from './Header';
import ProductItem from './ProductItem';
import Sidebar from './Sidebar';
import moment from 'moment';

export default function Products() {
  const dispatch = useDispatch()
  const [productList, setProductList] = useState([] as any);
  const [cartList, setcartList] = useState([] as any);
  const [sidebarToggle, setSidebarToggle] = useState(false);
  const { productsData } = useSelector((state: RootState) => state)

  const[filter,setFilter]=useState([] as any);

  const addToCart = (product: ProductType) => {
    setcartList([...cartList, product])
    setSidebarToggle(true)
    toast.success(`${product.name} added to cart Successfully`)
  }

  const deleteCartItem = (product: ProductType) => {
    let newCartList = cartList.filter((item: ProductType) => item.id !== product.id )
    setcartList(newCartList);
    toast.success(`${product.name} deleted from Successfully`)
  }

  useEffect(() => {
    dispatch(getProductsAction())
  }, [dispatch])
  
  useEffect(() => {
    setProductList(productsData?.data?.product)
    setFilter(productsData?.data?.product)
  }, [productsData])
  const[searching,setSearching]=useState({
    name:"",
    category:"",
    minPrice:number,
    maxPrice:number
  })

  const searchFilt=(search:any) => {
    let result = productList;
    console.log(result,"res")
    if (search.name) {
      result = result.filter((data) => {
        return data.name.toLowerCase().includes(search.name);
      });
    }
    if (search.category) {
      result = result.filter((data) => {
        return data.category.includes(search.category);
      });
    }
    if (search.Maxprice) {
      result = result.filter((data) => {
        return Number(data.price.split("$")[1]) <= search.Maxprice;
      });
    }
    if (search.Minprice) {
      result = result.filter((data) => {
        return Number(data.price.split("$")[1]) >= search.Minprice;
      });
    }
  
    if (search.MaxcreateDate) {
      result = result.filter((data) => {
        return (
          parseInt(data.createDate) <=
          Number(moment(search.MaxcreateDate).endOf("day"))
        );
      });
    }
    
    setSearching(search);
    setFilter(result);
  };

  const handleSearch = (event) => {
   console.log(event.target.value,"data")

    let value = event.target.value.toLowerCase();
    let name = event.target.name;
    let search = { ...searching };
    search[name] = value;

    searchFilt(search);
  };


  return (
    <>
      <div className="container">
        {sidebarToggle && <Sidebar 
          sidebarToggle={sidebarToggle} 
          setsidebarToggle={setSidebarToggle} 
          cartList={cartList} 
          deleteCartItem={deleteCartItem}
        />}

        <Header cartOpen={setSidebarToggle} handleSearch={handleSearch} />
        
        <h1 className='heading justify-content-center' >Our Products</h1>
        <hr />
        
        <div className='row'>
          {
            filter?.map((product: ProductType) => {
              return (
                <div className='col-3 mb-4 '>
                 <ProductItem product={product} addToCart={addToCart} />
                 </div>
              )
            })
          }
          </div>
         
      </div>
    </>
  )
};
