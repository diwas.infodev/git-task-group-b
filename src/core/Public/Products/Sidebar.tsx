import React from 'react';
import { useHistory } from 'react-router-dom';
import { ProductType } from 'store/modules/products/products';
import './sidebar.scss';
interface Props {
    sidebarToggle: boolean;
    setsidebarToggle: (state: boolean) => void;
    cartList: any;
    deleteCartItem: any
}
const Sidebar = (props: Props) => {
    const { sidebarToggle, setsidebarToggle, cartList } = props;
    const toggleidebar = () => setsidebarToggle(!sidebarToggle);
    const history = useHistory()

    return (
        <aside className="sidebar pt-1">
            <div className="d-flex justify-content-between align-items-center my-3">
                <h3 className="text-white ml-2 sidebar-brand">E-Commerce</h3>
                    <span style={{cursor: "pointer"}}className="ic-close text-white" onClick={toggleidebar}></span>
            </div>

            <div className="d-flex">
                <div className="p-2 text-white">My Cart</div>
                {cartList.length !== 0 && 
                <div className="ml-auto p-2">
                    <button className='btn btn-primary' onClick={() => history.push('/checkout')}>Make Payment</button>
                </div>
                }
            </div>

            <ul className="list list-sidebar mt-2">
                {
                    cartList.length === 0 ? <p className='ml-2 text-white mt-3'>No Products in Cart</p> :
                    cartList?.map((product: ProductType) => (
                        <li >
                            <div className="bg-light mt-1">
                                <div className="row p-2 mb-2">
                                    <div className="col-9">
                                        <h6>{product.name}</h6>
                                        <button className='btn btn-warning btn-sm'>{product.price}</button>
                                    </div>
                                    <div className="col-3 my-auto">
                                        <button onClick={() => props.deleteCartItem(product)} className='btn btn-outline-danger'><i className='ic ic-delete'></i></button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))
                }
            </ul>
            
        </aside>
    );
};

export default Sidebar;