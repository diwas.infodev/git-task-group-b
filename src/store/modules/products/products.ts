import { Dispatch } from "redux";

import { AppThunk } from "../..";
import { apiList } from "../../actionNames";
import initDefaultAction from "../../helper/default-action";
import initDefaultReducer from "../../helper/default-reducer";
import initialState from "../../helper/default-state";

export type ProductType = {
    "id": number,
        "name": string,
        "image": string,
        "price": string,
        "stock": number,
        "createDate": number,
        "category": string[]
}

type ProductsResponse = {
    "product": ProductType[]
}

const initialProductsState = initialState;
const apiDetails = Object.freeze(apiList.public.getProducts);

export default function productsReducer(state = initialProductsState, action: DefaultAction): DefaultState<ProductsResponse> {
    const stateCopy = Object.assign({}, state);
    const actionName = apiDetails.actionName;

    return initDefaultReducer(actionName, action, stateCopy);
}

export const getProductsAction = (): AppThunk<Promise<ProductsResponse>> => async (dispatch: Dispatch) => {
    return await initDefaultAction(apiDetails, dispatch, { disableSuccessToast: true });
};